#include "ConsoleApp.h"

#include <iostream>

ConsoleApp::ConsoleApp()
    : m_CurrentMaze(0,0)
{
}

ConsoleApp::~ConsoleApp()
{
}

int ConsoleApp::exec()
{
    bool bContinue = true;

    while(bContinue)
    {
        int choice;
        int width;
        std::string sFilename;
        std::vector<Coordinate> path;

        std::cout << "Enter choice : [ 0 : Quit | 1 : Generate | 2 : Save | 3 : Load | 4 : Solve ]" << std::endl;
        std::cin >> choice;

        switch(choice)
        {
        case 0:
            bContinue = false;
            break;
        case 1:
            std::cout << "Enter the width of the maze : ";
            std::cin >> width;
            if (width)
                m_CurrentMaze = m_App.generate(width, width);
            if (m_CurrentMaze.width())
                printMaze(m_CurrentMaze);
            break;
        case 2:
            std::cout << "Enter the filename to use : ";
            std::cin >> sFilename;
            if (m_App.save(sFilename, m_CurrentMaze))
                std::cout << "Successfuly saved" << std::endl;
            break;
        case 3:
            std::cout << "Enter the filename to use : ";
            std::cin >> sFilename;
            if (m_App.load(sFilename, m_CurrentMaze))
            {
                std::cout << "Successfuly loaded" << std::endl;
                printMaze(m_CurrentMaze);
            }
            break;
        case 4:
            path = m_App.solve(m_CurrentMaze);
            for (unsigned int i = 0; i < path.size(); i++)
                std::cout << "[" << path[i].x << ":" << path[i].y << "]";
            std::cout << std::endl;
            break;
        default:
            std::cout << "Unknow command" << std::endl;
            break;
        }
    }
    std::cout << "Bye !" << std::endl;
    return 0;
}
