#ifndef CONSOLEAPP_H
#define CONSOLEAPP_H

#include "MazeApp.h"

class ConsoleApp
{
public:
    ConsoleApp();
    ~ConsoleApp();

    int exec();

private:
    MazeApp m_App;
    Maze m_CurrentMaze;
};

#endif // CONSOLEAPP_H
