#include "MainWindow.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), m_CurrentMaze(0, 0)
{
    m_Layout = new QGridLayout(this);
    m_CentralWidget = new QWidget(this);
    m_View = new QGraphicsView(m_CentralWidget);
    m_Scene = new QGraphicsScene(0, 0, 500, 500, m_CentralWidget);

    m_GenerateBt = new QPushButton("Generate");
    m_SaveBt = new QPushButton("Save");
    m_LoadBt = new QPushButton("Load");
    m_SolveBt = new QPushButton("Solve");

    delete centralWidget();
    setCentralWidget(m_CentralWidget);

    delete m_CentralWidget->layout();
    m_CentralWidget->setLayout(m_Layout);

    delete m_View->scene();
    m_View->setScene(m_Scene);

    m_Layout->addWidget(m_View, 0, 0, 1, 4);

    m_Layout->addWidget(m_GenerateBt, 2, 0);
    m_Layout->addWidget(m_SaveBt, 2, 1);
    m_Layout->addWidget(m_LoadBt, 2, 2);
    m_Layout->addWidget(m_SolveBt, 2, 3);

    connect (m_GenerateBt, SIGNAL(clicked()), this, SLOT(generateNewMaze()));
    connect (m_SaveBt, SIGNAL(clicked()), this, SLOT(saveMaze()));
    connect (m_LoadBt, SIGNAL(clicked()), this, SLOT(loadMaze()));
    connect (m_SolveBt, SIGNAL(clicked()), this, SLOT(solveMaze()));
}

MainWindow::~MainWindow()
{
    delete m_CentralWidget;
}

void MainWindow::generateNewMaze()
{
    int width = QInputDialog::getInt(this, tr("Enter Width"), tr("width :"), 0, 0, 200);
    // int height = QInputDialog::getInt(this, tr("Enter Height"), tr("height :"));

    if (width > 0)
        m_CurrentMaze = m_mA.generate(width, width);
    printMaze();
}

void MainWindow::saveMaze()
{
    if (m_CurrentMaze.width() > 1 && m_CurrentMaze.height() > 1)
    {
        QString filename = QFileDialog::getSaveFileName(this, tr("FileName"));

        if (filename.size())
            m_mA.save(filename.toStdString(), m_CurrentMaze);
    }
}
void MainWindow::loadMaze()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("FileName"));

    if (m_mA.load(filename.toStdString(), m_CurrentMaze))
        printMaze();
}

void MainWindow::solveMaze()
{
    m_Path = m_mA.solve(m_CurrentMaze);

    if (m_Path.size())
        printPath();
}

void MainWindow::printMaze()
{
    m_Scene->clear();

    int tWidth = m_CurrentMaze.width();
    int tHeight = m_CurrentMaze.height();

    for (int x = 0; x < tWidth; x++)
    {
        for (int y = 0; y < tHeight; y++)
        {
            QPen p = (m_CurrentMaze.get(x, y).isWall()) ? QPen(Qt::black, 1) : QPen(Qt::white, 1);
            QBrush b = (m_CurrentMaze.get(x, y).isWall()) ? Qt::black : Qt::white;
            m_Scene->addRect(x * 500 / tWidth, y * 500 / tHeight, 500 / tWidth, 500 / tHeight, p, b);
        }
    }

    m_Scene->update();
}

void MainWindow::printPath()
{
    int tWidth = m_CurrentMaze.width();
    int tHeight = m_CurrentMaze.height();

    for (uint i = 0; i < m_Path.size(); i++)
    {
        QPen p(Qt::red, 1);
        QBrush b(Qt::red);
        m_Scene->addRect(m_Path[i].x * 500 / tWidth, m_Path[i].y * 500 / tHeight, 500 / tWidth, 500 / tHeight, p, b);
    }

    m_Scene->update();
}

