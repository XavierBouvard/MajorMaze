#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include "MazeApp.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void generateNewMaze();
    void saveMaze();
    void loadMaze();
    void solveMaze();

private:
    void printMaze();
    void printPath();

    MazeApp m_mA;
    Maze m_CurrentMaze;
    std::vector<Coordinate> m_Path;

    QWidget* m_CentralWidget;
    QGridLayout* m_Layout;

    QPushButton* m_GenerateBt;
    QPushButton* m_SaveBt;
    QPushButton* m_LoadBt;
    QPushButton* m_SolveBt;

    QGraphicsView* m_View;
    QGraphicsScene* m_Scene;
};

#endif // MAINWINDOW_H
