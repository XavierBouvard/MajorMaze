QT += core gui xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

OTHER_FILES += \
    MajorMaze.pro.user

HEADERS += \
    ConsoleApp.h \
    MainWindow.h \
    Maze.h \
    MazeApp.h \
    MazeFileIO.h \
    MazeGenerator.h \
    MazeSolver.h \
    Utils.h

SOURCES += \
    ConsoleApp.cpp \
    main.cpp \
    MainWindow.cpp \
    Maze.cpp \
    MazeFileIO.cpp \
    MazeGenerator.cpp \
    MazeSolver.cpp \
    Utils.cpp
