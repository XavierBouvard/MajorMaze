#include "Maze.h"

Maze::Maze(int width, int height)
{
    m_Cells.resize ((width * 2) + 1);

    for (int i = 0; i < (width * 2) + 1; i++)
    {
        m_Cells[i].resize((height * 2) + 1);
    }
}

Maze::Maze(const Maze &m)
{
    m_Cells = m.m_Cells;
}

Maze::~Maze ()
{

}

Cell & Maze::get(int x, int y)
{
    m_Cells[x][y].setCoordinate(x, y);
    return m_Cells[x][y];
}

Cell & Maze::get(Coordinate pos)
{
    m_Cells[pos.x][pos.y].setCoordinate(pos);
    return m_Cells[pos.x][pos.y];
}

const Cell & Maze::get(int x, int y) const
{
    // m_Cells[x][y].setCoordinate(x, y);
    return m_Cells[x][y];
}

const Cell & Maze::get(Coordinate pos) const
{
    // m_Cells[pos.x][pos.y].setCoordinate(pos);
    return m_Cells[pos.x][pos.y];
}

square Maze::getSquare() const
{
    square ret;

    ret.pt1.x = ret.pt1.y = 0;
    ret.pt2.x = width ();
    ret.pt2.y = height ();

    return ret;
}

int Maze::width () const
{
    return m_Cells.size ();
}

int Maze::height () const
{
    return m_Cells[0].size();
}

void Maze::setSizeRaw (int width, int height)
{
    m_Cells.resize (width);

    for (int i = 0; i < width; i++)
    {
        m_Cells[i].resize(height);
    }
}
