#ifndef MAZE_H
#define MAZE_H

#include <iostream>
#include <vector>

#include "Utils.h"

typedef Coordinate size;
typedef Line square;

class Maze
{
public:
    Maze(int width, int height);
    Maze(const Maze & m);
    ~Maze();

    Cell & get(int x, int y);
    Cell & get(Coordinate pos);

    const Cell & get(int x, int y) const;
    const Cell & get(Coordinate pos) const;

    square getSquare() const;
    int width() const;
    int height() const;

    void setSizeRaw(int width, int height);

private:
    std::vector<std::vector<Cell> > m_Cells;
};

inline void printMaze(const Maze & m)
{
    square sq = m.getSquare ();

    for (int y = sq.pt1.y; y < sq.pt2.y; y++)
    {
        std::cout << '|';
        for (int x = sq.pt1.x; x < sq.pt2.x; x++)
        {
            std::cout << ((m.get(x, y).isWall()) ? '+' : ' ');
        }
        std::cout << '|';
        std::cout << std::endl;
    }
}

#endif // MAZE_H
