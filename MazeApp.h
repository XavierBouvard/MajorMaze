#ifndef MAZEAPP_H
#define MAZEAPP_H

#include "Maze.h"
#include "MazeGenerator.h"
#include "MazeFileIO.h"
#include "MazeSolver.h"

class MazeApp
{
public:
    MazeApp() { ; }
    ~MazeApp() { ; }

    Maze generate(int width, int height) { return m_Generator.generateMaze (width, height); }
    bool load(const std::string & filename, Maze & maze) { return m_FileManager.load (filename, maze); }
    bool save(const std::string & filename, const Maze & maze) { return m_FileManager.save (filename, maze); }
    std::vector<Coordinate> solve(const Maze & m) { return m_Solver.solve (m); }

private:
    MazeGenerator m_Generator;
    MazeFileIO m_FileManager;
    MazeSolver m_Solver;
};

#endif // MAZEAPP_H
