#include "MazeFileIO.h"

/* ---------- MAZEFILELOADER ---------- */
MazeFileLoader::MazeFileLoader()
    : m_Maze(0, 0)
{

}

MazeFileLoader::~MazeFileLoader()
{
    if (m_File.is_open())
        m_File.close();
}

bool MazeFileLoader::load(const std::string & filename, Maze & maze)
{
    bool ret = false;

    m_sFilename = filename;
    m_File.open(m_sFilename.c_str());

    if (m_File.is_open())
    {
        ret = read();
        maze = m_Maze;
        m_File.close();
    }

    return ret;
}

bool MazeFileLoader::read()
{
    int width, height;

    m_File.read(reinterpret_cast<char*>(&width), sizeof(int));
    m_File.read(reinterpret_cast<char*>(&height), sizeof(int));

    m_Maze.setSizeRaw(width, height);

    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            m_Maze.get(x, y).isWall(m_File.get());
        }
    }

    return m_File.good();
}

/* ---------- MAZEFILEWRITTER ---------- */

MazeFileWritter::MazeFileWritter()
    : m_Maze(0, 0)
{
}

MazeFileWritter::~MazeFileWritter ()
{
    if (m_File.is_open ())
        m_File.close ();
}

bool MazeFileWritter::save (const std::string &filename, const Maze &maze)
{
    bool ret = false;
    m_sFilename = filename;
    m_Maze = maze;

    m_File.open (m_sFilename.c_str ());

    if (m_File.is_open ())
    {
        ret = write();
        m_File.close ();
    }

    return ret;
}

bool MazeFileWritter::write ()
{
    int width = m_Maze.width ();
    int height = m_Maze.height ();

    m_File.write (reinterpret_cast<char*>(&width), sizeof(int));
    m_File.write (reinterpret_cast<char*>(&height), sizeof(int));

    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            m_File.put (m_Maze.get (x, y).isWall ());
        }
    }

    return m_File.good();
}

/* ---------- MazeFileIO ---------- */
MazeFileIO::MazeFileIO() { }
MazeFileIO::~MazeFileIO() { }

bool MazeFileIO::load(const std::string & filename, Maze & maze) { return m_Loader.load(filename, maze); }
bool MazeFileIO::save(const std::string & filename, const Maze & maze) { return m_Writter.save(filename, maze); }
