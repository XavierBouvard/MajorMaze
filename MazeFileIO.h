#ifndef MAZEFILEIO_H
#define MAZEFILEIO_H

#include <fstream>
#include <string>

#include "Maze.h"

class MazeFileLoader
{
public:
    MazeFileLoader();
    ~MazeFileLoader();

    bool load(const std::string & filename, Maze & maze);

private:
    bool read();

    std::string m_sFilename;
    std::ifstream m_File;
    Maze m_Maze;
};

class MazeFileWritter
{
public:
    MazeFileWritter();
    ~MazeFileWritter();

    bool save(const std::string & filename, const Maze & maze);

private:
    bool write();

    std::string m_sFilename;
    std::ofstream m_File;
    Maze m_Maze;
};

class MazeFileIO {
public:
    MazeFileIO();
    ~MazeFileIO();

    bool load(const std::string & filename, Maze & maze);
    bool save(const std::string & filename, const Maze & maze);

private:
    MazeFileWritter m_Writter;
    MazeFileLoader m_Loader;
};

#endif // MAZEFILEWRITTER_H
