#include "MazeGenerator.h"

#include <ctime>

MazeGenerator::MazeGenerator()
    : m_Maze(0, 0)
{
    srand (time(NULL));
}

MazeGenerator::~MazeGenerator ()
{

}

Maze MazeGenerator::generateMaze (int width, int height)
{
    m_Maze = Maze(width, height);

    init();
    proceed ();

    return m_Maze;
}

void MazeGenerator::init ()
{
    square sq = m_Maze.getSquare ();

    for (int x = 0; x < sq.pt2.x; x++)
    {
        for (int y = 0; y < sq.pt2.x; y++)
        {
            if (!(y%2) || !(x%2))
                m_Maze.get(x, y).isWall(true);
        }
    }

    sq.pt2.x--;
    sq.pt2.y--;

    // generate maze entries
    // sometimes generate the exit as the entrance. The maze is then unsolvable.
    for (int i = 0; i < 2; i++)
    {
        int choice = rand() % 4;
        Coordinate coords;

        switch (choice)
        {
        case 0:
            coords.x = sq.pt1.x;
            while((coords.y = (rand() % (sq.pt2.y - sq.pt1.y - 1) + sq.pt1.y + 1)) % 2 != 1);
            break;
        case 1:
            coords.y = sq.pt1.y;
            while((coords.x = (rand() % (sq.pt2.x - sq.pt1.x - 1) + sq.pt1.x + 1)) % 2 != 1);
            break;
        case 2:
            coords.x = sq.pt2.x;
            while((coords.y = (rand() % (sq.pt2.y - sq.pt1.y - 1) + sq.pt1.y + 1)) % 2 != 1);
            break;
        case 3:
            coords.y = sq.pt2.y;
            while((coords.x = (rand() % (sq.pt2.x - sq.pt1.x - 1) + sq.pt1.x + 1)) % 2 != 1);
            break;
        }

        m_Maze.get(coords).isWall(false);
    }

    m_PendingSquares.push_back(sq);
}

void MazeGenerator::proceed ()
{
    while (m_PendingSquares.size ())
    {
        square currentSquare = m_PendingSquares.back();
        m_PendingSquares.pop_back();

        int width = currentSquare.pt2.x - currentSquare.pt1.x;
        int height = currentSquare.pt2.y - currentSquare.pt1.y;

        if ((width > 1 && height > 2) || (height > 1 && width > 2))
        {
            int x = 0;
            int y = 0;
            Line l;

            x = (rand() % width) + currentSquare.pt1.x;
            y = (rand() % height) + currentSquare.pt1.y;

            while (
                   x <= currentSquare.pt1.x || y <= currentSquare.pt1.y ||
                   x >= currentSquare.pt2.x || y >= currentSquare.pt2.y ||
                   (x % 2 == y % 2) ||
                   !m_Maze.get(x, y).isWall()
                   )
            {
                x = (rand() % width) + currentSquare.pt1.x;
                y = (rand() % height) + currentSquare.pt1.y;
            }

            if (!(x % 2) && (y % 2))
            {
                l.pt1.y = currentSquare.pt1.y;
                l.pt2.y = currentSquare.pt2.y;
                l.pt1.x = l.pt2.x = x;
            }
            else if ((x % 2) && !(y % 2))
            {
                l.pt1.x = currentSquare.pt1.x;
                l.pt2.x = currentSquare.pt2.x;
                l.pt1.y = l.pt2.y = y;
            }

            m_Maze.get(x, y).isWall(false);

            square newSquare1;
            newSquare1.pt1 = currentSquare.pt1;
            newSquare1.pt2 = l.pt2;
            m_PendingSquares.push_back(newSquare1);

            square newSquare2;
            newSquare2.pt1 = l.pt1;
            newSquare2.pt2 = currentSquare.pt2;

            m_PendingSquares.push_back(newSquare2);
        }
    }
}
