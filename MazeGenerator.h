#ifndef MAZEGENERATOR_H
#define MAZEGENERATOR_H

#include "Maze.h"
#include <cstdlib>

class MazeGenerator
{
public:
    MazeGenerator();
    ~MazeGenerator();

    Maze generateMaze(int width, int height);

private:
    void init();
    void proceed();

    std::vector<square> m_PendingSquares;
    Maze m_Maze;
};

#endif // MAZEGENERATOR_H
