#include "MazeSolver.h"

#include <ctime>

MazeSolver::MazeSolver()
    : m_Maze(0, 0)
{
    srand (time(NULL));
}

MazeSolver::~MazeSolver()
{

}

std::vector<Coordinate> MazeSolver::solve (const Maze &m)
{
    m_Maze = m;

    if (init () && procced ())
        generatePath ();

    return m_Path;
}

bool MazeSolver::init ()
{
    m_PendingNode.clear ();
    m_TreadtedNode.clear ();
    m_Path.clear ();

    int width = m_Maze.width ();
    int height = m_Maze.height ();

    std::vector<Node> entries;

    for (int x = 0; x < width; x++)
    {
        if (!m_Maze.get (x, 0).isWall ())
            entries.push_back (Node(m_Maze.get (x, 0)));
        if (!m_Maze.get (x, height-1).isWall ())
            entries.push_back (Node(m_Maze.get (x, height-1)));
    }

    for (int y = 0; y < height; y++)
    {
        if (!m_Maze.get (0, y).isWall ())
            entries.push_back (Node(m_Maze.get (0, y)));
        if (!m_Maze.get (width - 1, y).isWall ())
            entries.push_back (Node(m_Maze.get (width - 1, y)));
    }

    if (entries.size () == 2)
    {
        int index = rand() % 2;
        m_Entrance = entries[index];
        m_Exit = entries[(index+1)%2];

        m_Entrance.setWeight (0);
        m_Exit.setWeight (width * height);

        m_PendingNode.push_back (m_Entrance);

        return true;
    }

    return false;
}

bool MazeSolver::procced()
{
    bool ret = false;

    while (m_PendingNode.size ())
    {
        Node currentNode = m_PendingNode.back ();
        m_PendingNode.pop_back ();

        std::vector<Node> neighbors = findNeighbors(currentNode);

        for (unsigned int i = 0; i < neighbors.size (); i++)
        {
            neighbors[i].setWeight(currentNode.weight () + 1);
            if ((Cell)neighbors[i] == (Cell)m_Exit && neighbors[i].weight() < m_Exit.weight ())
            {
                ret = true;
                m_Exit = neighbors[i];
            }
            else
            {
                if (!update(neighbors[i]))
                    m_PendingNode.push_back (neighbors[i]);
            }
        }

        m_TreadtedNode.push_back (currentNode);
    }

    return ret;
}


void MazeSolver::generatePath ()
{
    m_Path.clear ();
    Node currentNode = m_Exit;
    m_Path.push_back(currentNode.pos ());

    while (!(currentNode == m_Entrance))
    {
        Node tmpNode;
        tmpNode.setWeight (m_Maze.width () * m_Maze.height());

        for (unsigned int i = 0; i < m_TreadtedNode.size (); i++)
        {
            if (areNeighbors(currentNode, m_TreadtedNode[i]))
            {
                if (m_TreadtedNode[i].weight() < tmpNode.weight ())
                {
                    tmpNode = m_TreadtedNode[i];
                }
            }
        }

        currentNode = tmpNode;
        m_Path.push_back(tmpNode.pos ());
    }
}

std::vector<Node> MazeSolver::findNeighbors (const Node &n)
{
    std::vector<Node> ret;
    Coordinate c = n.pos (), tmp = c;
    square sq = m_Maze.getSquare ();

    tmp.x = c.x -1;
    tmp.y = c.y;
    if (inRange (sq, tmp) && !m_Maze.get (tmp).isWall ())
        ret.push_back (m_Maze.get (tmp));

    tmp.x = c.x +1;
    tmp.y = c.y;
    if (inRange (sq, tmp) && !m_Maze.get (tmp).isWall ())
        ret.push_back (m_Maze.get (tmp));

    tmp.x = c.x;
    tmp.y = c.y -1;
    if (inRange (sq, tmp) && !m_Maze.get (tmp).isWall ())
        ret.push_back (m_Maze.get (tmp));

    tmp.x = c.x;
    tmp.y = c.y +1;
    if (inRange (sq, tmp) && !m_Maze.get (tmp).isWall ())
        ret.push_back (m_Maze.get (tmp));


    return ret;
}

bool MazeSolver::update (const Node &n)
{
    bool ret = false;

    for (unsigned int i = 0; i < m_PendingNode.size (); i++)
    {
        if ((Cell)n == (Cell)m_PendingNode[i])
        {
            if (n.weight () < m_PendingNode[i].weight())
                m_PendingNode[i] = n;
            ret = true;
        }
    }

    for (unsigned int i = 0; i < m_TreadtedNode.size (); i++)
    {
        if ((Cell)n == (Cell)m_TreadtedNode[i])
        {
            if (n.weight () < m_TreadtedNode[i].weight())
                m_TreadtedNode[i] = n;
            else ret = true;
        }
    }

    return ret;
}
