#ifndef MAZESOLVER_H
#define MAZESOLVER_H

#include "Maze.h"
#include <cstdlib>

class MazeSolver
{
public:
    MazeSolver();
    ~MazeSolver();

    std::vector<Coordinate> solve(const Maze & m);

private:
    bool init();
    bool procced();
    void generatePath();
    std::vector<Node> findNeighbors(const Node & n);
    bool update(const Node & n);

    Node m_Entrance;
    Node m_Exit;

    Maze m_Maze;

    std::vector<Node> m_PendingNode;
    std::vector<Node> m_TreadtedNode;

    std::vector<Coordinate> m_Path;
};

#endif // MAZESOLVER_H
