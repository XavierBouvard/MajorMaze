#include "Utils.h"

/* --------- CELL ---------- */

Cell::Cell()
{
    m_Pos.x = 0;
    m_Pos.y = 0;
    m_bIsWall = false;
}

Cell::Cell(const Cell & c)
{
    m_Pos = c.m_Pos;
    m_bIsWall = c.m_bIsWall;
}

Cell::~Cell ()
{

}

void Cell::setCoordinate (const Coordinate &pos)
{
    m_Pos = pos;
}

void Cell::setCoordinate (int x, int y)
{
    m_Pos.x = x;
    m_Pos.y = y;
}

bool Cell::isWall () const
{
    return m_bIsWall;
}

bool Cell::isWall (bool wall)
{
    return m_bIsWall = wall;
}

int Cell::x () const
{
    return m_Pos.x;
}

int Cell::y () const
{
    return m_Pos.y;
}

Coordinate Cell::pos () const { return m_Pos; }

/* ---------- NODE ---------- */

Node::Node(int weight)
{
    m_nWeight = weight;
}

Node::Node(const Node &n)
    : Cell(n)
{
    m_nWeight = n.m_nWeight;
}

Node::Node(const Cell &c)
    : Cell(c)
{

}

Node::~Node ()
{

}

void Node::setWeight (int weight)
{
    m_nWeight = weight;
}

int Node::weight () const
{
    return m_nWeight;
}
