#ifndef UTILS_H
#define UTILS_H

#include <math.h>

struct Coordinate {
    int x;
    int y;
};

struct Line {
    Coordinate pt1;
    Coordinate pt2;
};

class Cell {
public:
    Cell();
    Cell(const Cell & c);
    virtual ~Cell();

    bool operator==(const Cell & c)
    {
        return (m_Pos.x == c.m_Pos.x)
            && (m_Pos.y == c.m_Pos.y)
            && (m_bIsWall == c.m_bIsWall);
    }

    void setCoordinate(const Coordinate & pos);
    void setCoordinate (int x, int y);

    bool isWall(bool wall);
    bool isWall() const;

    int x() const;
    int y() const;
    Coordinate pos() const;

private:
    Coordinate m_Pos;
    bool m_bIsWall;
};

class Node : public Cell {
public:
    Node(int weight = 0);
    Node(const Node & n);
    Node(const Cell & c);
    virtual ~Node();

    bool operator==(const Node & n)
    {
        return (m_nWeight == n.m_nWeight)
            && Cell::operator == (n);
    }

    void setWeight(int weight);

    int weight() const;

private:
    int m_nWeight;
};

inline bool inRange(Line l, Coordinate c)
{
    return (l.pt1.x <= c.x && c.x < l.pt2.x)
        && (l.pt1.y <= c.y && c.y < l.pt2.y);
}

inline int distance(Coordinate pt1, Coordinate pt2)
{
    return sqrt (pow((pt2.x - pt1.x), 2) + pow((pt2.y - pt1.y), 2));
}

inline bool areNeighbors(const Cell & c1, const Cell & c2)
{
    if ((c1.x ()== c2.x () + 1 || c1.x ()== c2.x () - 1) && c1.y () == c2.y ())
        return true;
    else if ((c1.y ()== c2.y () + 1 || c1.y ()== c2.y () - 1) && c1.x () == c2.x ())
        return true;
    return false;
}

#endif // UTILS_H
