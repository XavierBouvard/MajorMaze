#include "MainWindow.h"
#include <QApplication>
#include "ConsoleApp.h"
#include <iostream>

#include "MazeApp.h"

int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "-c") == 0)
    {
        ConsoleApp a;
        return a.exec();
    }
    else
    {
        QApplication a(argc, argv);
        MainWindow w;
        w.show();
        return a.exec();
    }
}
